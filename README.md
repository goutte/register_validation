# Register Validation

The present repository contains the code associated with the article "[Development and validation of a self-updating gout register from electronic health records data](https://rmdopen.bmj.com/content/10/2/e004120.long)" by Nils Bürgisser, Denis Mongin, Samia Mehouachi, Clement P. Buclin, Romain Guemara, Pauline Darbellay Farhoumand, Olivia Braillard, Kim Lauper, Delphine S. Courvoisier.

The file [text_analysis.R](text_analysis.R) provides the functions to identify documents speaking about Gout in French together with a working example. 

The file `zugelassene_packungen_ham.xlsx` contains the list of all commercially autorized drug in Switzerland in June 2023, obtained on the official swissmedic website https://www.swissmedic.ch/swissmedic/en/home/services/listen_neu.html. The file `example.xlsx` contains 16 text of deidentified documents from the hospital data, selected as containin the words "goutte","tophus","tophace" or "podagre".

The process works with the following steps:

- identify documents with the words "goutte","tophus","tophace","podagre"
- split the document in sentences, and isolate sentences containg the above words. 
- normalise the text (remove accents, non asci characters etc), isolate the surrounding of the detected word (default is between -8 up to + 5 words around the detected word)
- in case of the word goutte, identify the presence of words indicating false negatives:
   - drugs that can be administrated as liquid drops
   - use of goutte to designate human body liquids
   - any French expression containing the word goutte but refering to something else than the disease
   - any use of goutte in diagnosis designing other disease (pseudo-goutte is for chondrocalcinosis)
- identify negation to detect potential negative diagnostics
- combine the previous tests to have a result for the whole document

The normalisation of text is performed thanks to the function `norm_name`, the extraction of the word surrounding by the function `extract_context`, and the analysis of the text by the function `identify_gout`.



